import requests
import json
from os import environ

#Environment Variables
gitlab_api_hostname=environ.get('GITLAB_API_HOSTNAME')
gitlab_access_token=environ.get('GITLAB_ACCESS_TOKEN')
gitlab_project=environ.get('GITLAB_PROJECT')

#Variables
headers={'PRIVATE-TOKEN': gitlab_access_token}
gitlab_mergerequests_api='{}/projects/{}/merge_requests'\
  .format(gitlab_api_hostname, gitlab_project)

#List all MergeRequest opened
mr_list = requests.get(gitlab_mergerequests_api + '?state=opened&approved=no', headers=headers)
for mr in mr_list.json():
  gitlab_mr_api=gitlab_mergerequests_api + '/{}'.format(mr['iid'])
  gitlab_approvals_api= gitlab_mr_api + '/approvals'
  
  # List Approvals Left
  approval_list = requests.get(gitlab_approvals_api, headers=headers)
  approval_rules_left = approval_list.json()['approval_rules_left']
  gitlab_notes_api= gitlab_mr_api + '/notes'
  
  for ar_left in approval_rules_left:
    if ar_left['rule_type'] == 'code_owner':

      # List Approval Rules
      gitlab_approval_rules_api= gitlab_mr_api + '/approval_rules/{}'.format(ar_left['id'])
      ar = requests.get(gitlab_approval_rules_api, headers=headers)
      approvers = ''
      for eligible_approver in ar.json()['eligible_approvers']:
        approvers = approvers + ' @{}'.format(eligible_approver['username'])
      note={
        'body': '👋 {}, approvals for \'{}\' are needed for this merge request'\
          .format(approvers, ar.json()['name'])
      }

      # Send a note
      response = requests.post(gitlab_notes_api, headers=headers, params=note)
      print(response.status_code)
